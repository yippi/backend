<?php

namespace iBrand\Backend\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\{Rule, Validator};

class LoginFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $table_name = 'ibrand_supplier';

        return  [
            'username' => [
                'required',
                Rule::exists($table_name, 'yippi_inbox')->where(function ($query) {
                    $query->where(['status' => 1]);
                })
            ],
            'password' => ['required'],
        ];
    }

    public function messages(): array
    {
        return [
            'username.exists' => ':attribute is freezed'
        ];
    }

    public function validateResolved(): array
    {
        return $this->validated();
    }
}
