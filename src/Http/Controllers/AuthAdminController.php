<?php

/*
 * This file is part of ibrand/backend.
 *
 * (c) iBrand <https://www.ibrand.cc>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace iBrand\Backend\Http\Controllers;

use DB;
use Auth;
use Cookie;
use Encore\Admin\Form;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
// use iBrand\Sms\Facade as Sms;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Facades\Hash,iBrand\Backend\Traits\LoginPageable,GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Validator;
use Encore\Admin\Controllers\AuthController;
use Encore\Admin\Auth\Database\Administrator;
use iBrand\Backend\Requests\LoginFormRequest;
use iBrand\Backend\Models\Admin as newAdminModel;
use GuoJiangClub\EC\Open\Backend\Store\Model\Supplier;

class AuthAdminController extends AuthController
{
    use LoginPageable;

    public function getLogin()
    {
        if ($this->guard()->check()) {
            return redirect($this->redirectPath());
        }

        $redirectTo = $this->isPathToRedirect();
        $file       = $this->isMerchantLogin() ?? $this->isAdminLogin();

        return view($file)->with(compact('redirectTo'));
    }

    public function postLogin(Request $request)
    {
        $username = !$this->isMail(request($this->username())) ? 'username' : 'email';

        $credentials = $request->only([$this->username(), 'password', 'code']);

        // FIXME: this function is for SMS login only
        // if (config('ibrand.backend.sms_login')) {
        //     $admin = Administrator::where("$username", request('username'))->where('status', 1)->first();

        //     if (!$admin or !$admin->mobile) {
        //         return redirect()->back()->withInput()->withErrors(['username' => '账号不存在或未绑定手机']);
        //     }

        //     $mobile = $admin->mobile;

        //     $credentials_code = [
        //         'mobile' => $mobile,
        //         'verifyCode' => request('code'),
        //     ];

        //     if (!request('code')) {
        //         return redirect()->back()->withInput()->withErrors(['code' => '验证码不能为空']);
        //     }

        //     if (!Sms::checkCode($mobile, \request('code'))) {
        //         return redirect()->back()->withInput()->withErrors(['code' => '验证码不正确']);
        //     }

        //     unset($credentials['code']);
        // }

        $validator = Validator::make($credentials, [
            $this->username() => 'required',
            'password'        => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $new_credentials[$username]  = $credentials[$this->username()];
        $new_credentials['password'] = $credentials['password'];

        if ($this->guard()->attempt($new_credentials)) {
            return $this->sendEShopLoginResponse($request);
        }

        return back()->withInput()->withErrors([
            $this->username() => $this->getFailedLoginMessage(),
        ]);
    }

    /**
     * Merchant Login for Eshop
     *
     * @param  mixed $request
     * @return void
     */
    public function yippiPostLogin(LoginFormRequest $request)
    {
        $validatedRequest = $request->only(['username', 'password']);

        try {
            $client  = new Client();
            $request = $client->request('POST', config('login.live.api_url'), [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'form_params' => $validatedRequest,
            ]);

            $response = json_decode($request->getBody(), true);

            if (is_null($response['access_token'])) {
                return back()->withInput()->withErrors([
                    $this->username() => $this->getFailedLoginMessage(),
                ]);
            }

            //if login API success
            $admin = $this->checkAdminUserDB($response['username'], $validatedRequest['password']);

            if (!$admin->exists()) {
                return back()->withInput()->withErrors([
                    $this->username() => $this->getFailedLoginMessage(),
                ]);
            }

            $supplier = DB::table('ibrand_supplier')->where('admin_id', $admin->id)->exists();

            if ($supplier) {
                // $params = [
                //     'email'    => $admin->email,
                //     'password' => $validatedRequest['password'],
                //     'status'   => 1,
                // ];

                // $this->guard()->attempt($params, true);
                $this->guard()->login($admin, true);

                request()->session()->regenerate();

                return ['status' => true, 'path' => '/store/dashboard'];
            }
        } catch (GuzzleException $e) {
            $response = $e->getResponse()->getBody()->getContents();

            \Log::error('ERROR RESPONSE FROM YIPPI LOGIN: ', [$response]);

            return response()->json(json_decode($response), $e->getCode());
        }
    }

    /**
     * Check Yippi username in admin_users table.
     *
     * @param  mixed $array
     * @return void
     */
    public function checkAdminUserDB(string $yippi_username, string $password = '0'): Administrator
    {
        $user = Administrator::where('yippi_username', '=', $yippi_username)
                ->where('email', 'like', 'login@yippi.com%')
                ->first();

        // if (!Hash::check($password, $user->password)) {
        //     $user->password = Hash::make($password);
        //     $user->save();
        // }

        // if(!$user){             // yippi_username not exists in DB
        //     $checkDuplicate = Administrator::where('yippi_username', '=', $yippi_username)->first();

        //     if(is_null($checkDuplicate)){
        //         $id   = newAdminModel::count();
        //         $user = newAdminModel::create([
        //             'name'              => $yippi_username,
        //             'username'          => $yippi_username,
        //             'yippi_username'    => $yippi_username,
        //             'email'             => 'login@yippi.com.'.$id,
        //             'main_role'         => 2,
        //             'supplier_id'       => 0,
        //             'password'          => $password,
        //         ]);

        //         DB::table('admin_role_users')->insert([
        //             'role_id' => 3,
        //             'user_id' => $user->id,
        //         ]);

        //     }

        // // NOTIFICATION
        //     $content =  "(System) Welcome to Yippi EShop Marketplace! \r\n".
        //                 "If you have any enquiry regarding about Yippi EShop Marketplace. \r\n".
        //                 "Kindly contact us via Yippi App Customer Support for assistance. Thank you. \r\n".
        //                 "\r\n".
        //                 "(系统)您好！欢迎来到 Yippi EShop 电商平台！\r\n".
        //                 "如果您对Yippi EShop Marketplace有任何疑问。\r\n".
        //                 "请通过 Yippi 应用里的客服服务与我们联系哦。 谢谢\r\n";
        //     $this->sendYippiMsg($yippi_username, $content);
        // }
        return $user;
    }

    public function getMobile(Request $request)
    {
        $username = !$this->isMail(request($this->username())) ? 'username' : 'email';

        if ($user = Administrator::where("$username", $request->username)->where('status', 1)->first() and $user->mobile) {
            return response()->json([
                'data'   => $user,
                'status' => true,
            ]);
        }

        return response()->json([
            'msg'    => '管理员账号不存在或未绑定手机号码',
            'status' => false,
        ]);
    }

    public function getLogout(Request $request)
    {
        if (config('ibrand.backend.scenario') == 'normal' || !config('ibrand.backend.scenario')) {
            $this->guard()->logout();

            $request->session()->invalidate();

            return redirect('/');
        }

        $this->guard()->logout();
        Auth::guard('account')->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        Cookie::queue(Cookie::forget('ibrand_log_uuid'));
        Cookie::queue(Cookie::forget('ibrand_log_sso_user'));
        Cookie::queue(Cookie::forget('ibrand_log_application_name'));
        Cookie::queue(Cookie::forget('ibrand_log_sso_user'));

        return redirect('/account/login');
    }

    /**
     * Get the post login redirect path.
     *
     * @return string
     */
    protected function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : config('admin.route.prefix');
    }

    protected function isMail($Argv)
    {
        $RegExp = '/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/';

        return preg_match($RegExp, $Argv) ? $Argv : false;
    }

    public function getSetting(Content $content)
    {
        return Admin::content(function (Content $content) {
            $content->header(trans('admin.user_setting'));
            $form = $this->settingForm();
            $form->tools(
                function (Form\Tools $tools) {
                    $tools->disableBackButton();
                    $tools->disableListButton();
                }
            );
            $content->body($form->edit(Admin::user()->id));
        });
    }

    /**
     * Update user setting.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putSetting()
    {
        return $this->settingForm()->update(Admin::user()->id);
    }

    /**
     * Model-form for user setting.
     *
     * @return Form
     */
    protected function settingForm()
    {
        return Administrator::form(function (Form $form) {
            $form->display('username', trans('admin.username'));
            $form->text('name', trans('admin.name'))->rules('required');
            $form->email('email', trans('Email'));
            $form->mobile('mobile', trans('Mobile'));
            $form->image('avatar', trans('admin.avatar'));
            $form->password('password', trans('admin.password'))->rules('confirmed|required');
            $form->password('password_confirmation', trans('admin.password_confirmation'))->rules('required')
                ->default(function ($form) {
                    return $form->model()->password;
                });

            $form->setAction(admin_base_path('auth/setting'));

            $form->ignore(['password_confirmation']);

            $form->saving(function (Form $form) {
                if ($form->password && $form->model()->password != $form->password) {
                    $form->password = bcrypt($form->password);
                }
            });

            $form->saved(function () {
                admin_toastr(trans('admin.update_succeeded'));

                return redirect(admin_base_path('auth/setting'));
            });
        });
    }

    public function sendYippiMsg($to, $message, $from = null)
    {
        $url  = 'https://api.netease.im/nimserver/msg/sendMsg.action';
        $data = [
            'from'        => $from != null ? $from : 'eshopnoreply',
            'ope'         => '0',
            'to'          => $to,
            'type'        => '0',
            'body'        => json_encode(['msg' => $message]),
            'option'      => json_encode(['push' => false, 'roam' => true, 'history' => true, 'sendersync' => true, 'route' => false]),
            'pushcontent' => '',
        ];
        $this->postDataCurl($url, $data);
    }

    public function postDataCurl($url, $data)
    {
        $AppKey     = '6153c6c22f23b18d1e970d16397aefbd';
        $AppSecret  = 'eb163332b6f7';
        $hex_digits = '0123456789abcdef';
        $Nonce      = '';
        for ($i = 0; $i < 128; $i++) {
            $Nonce .= $hex_digits[rand(0, 15)];
        }
        $CurTime     = (string) (time()); //当前时间戳，以秒为单位
        $join_string = $AppSecret . $Nonce . $CurTime;
        $CheckSum    = sha1($join_string);

        $timeout     = 5000;
        $http_header = [
            'AppKey:' . $AppKey,
            'Nonce:' . $Nonce,
            'CurTime:' . $CurTime,
            'CheckSum:' . $CheckSum,
            'Content-Type:application/x-www-form-urlencoded;charset=utf-8',
        ];
        $postdataArray = [];
        foreach ($data as $key => $value) {
            array_push($postdataArray, $key . '=' . urlencode($value));
        }
        $postdata = join('&', $postdataArray);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //处理http证书问题
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        if (false === $result) {
            $result = curl_errno($ch);
        }
        curl_close($ch);
        //return $this->json_to_array($result) ;
    }

    /**
     * Model-form for user login as.
     *
     * @return Form
     */
    public function loginAs(Request $request)
    {
        $admin_info         = Administrator::find($request->get('admin_id'));
        $master_admin_id    = Admin::user()->id;
        $master_user_locale = Admin::user()->user_locale;

        $supplier       = Supplier::with('Country', 'admin')->where('admin_id', $request->get('admin_id'))->first();
        $supID[]        = $supplier->id;
        $supCountryInfo = [];
        if ($supplier->Country) {
            $supCountryInfo = $supplier->Country->toArray();
        }

        if (!$admin_info) {
            return $this->ajaxJson(false, [], 300, trans('admin.failed_login_as'));
        }

        $info = $this->guard()->loginUsingId($admin_info->id, true);

        if (!$info) {
            return $this->ajaxJson(false, [], 300, trans('admin.failed_login_as'));
        }

        $request->session()->flush();
        $request->session()->regenerate();

        $request->session()->put('master_admin_id', $master_admin_id);
        $request->session()->put('user_locale', $master_user_locale);
        $request->session()->put('login_as_supplier', true);
        $request->session()->put('login_as_supplier_id', $supID);
        $request->session()->put('login_as_country_info', $supCountryInfo);

        return $this->ajaxJson(true, [], 200, trans('admin.login_as_successfully'));
    }

    public function logoutAs(Request $request)
    {
        $admin_id    = session()->get('master_admin_id');
        $user_locale = session()->get('user_locale');

        $info = $this->guard()->loginUsingId($admin_id, true);

        if (!$info) {
            return $this->ajaxJson(false, [], 300, trans('admin.failed_logout_as'));
        }

        $request->session()->flush();
        $request->session()->regenerate();

        $request->session()->put('user_locale', $user_locale);

        return $this->ajaxJson(true, [], 200, trans('admin.logout_as_successfully'));
    }

    private function ajaxJson($status = true, $data = [], $code = 200, $message = '')
    {
        return response()->json(
            ['status' => $status, 'code' => $code, 'message' => $message, 'data' => $data]
        );
    }

    public function changeLocale()
    {
        $admin_id         = Admin::user()->id;
        $available_locale = config('admin.available_locale');

        return view('admin::partials.change_locale', compact('available_locale'));
    }

    public function saveLocale(Request $request)
    {
        if (!$request['user_locale']) {
            return $this->ajaxJson(false, [], 300, trans('admin.invalid_locale'));
        }

        try {
            DB::beginTransaction();

            $upd                = [];
            $upd['user_locale'] = $request['user_locale'];

            newAdminModel::where('id', Admin::user()->id)->update($upd);
            DB::commit();

            // Set the language
            $request->session()->put('user_locale', $request['user_locale']);

            return $this->ajaxJson();
        } catch (\Exception $exception) {
            DB::rollBack();

            \Log::info($exception->getMessage() . $exception->getTraceAsString());

            return $this->ajaxJson(false, [], 300, trans('admin.invalid_locale'));
        }
    }

    /**
     * Get the post login redirect path.
     *
     * @return string
     */
    protected function redirectEShopPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : config('admin.route.prefix');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    protected function sendEShopLoginResponse(Request $request)
    {
        admin_toastr(trans('admin.login_successful'));

        $request->session()->regenerate();

        $admin = Admin::user();

        if ($admin->id == 1) {
            return redirect()->intended($this->redirectEShopPath());
        } else {
            return redirect()->route('admin.store.dashboard.index');
        }
    }
}
