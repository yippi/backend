<?php

namespace iBrand\Backend\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Encore\Admin\Facades\Admin as LaravelAdmin;
use Encore\Admin\Layout\Content;

class ManualController extends Controller
{
    const FILE_PATHNAME = 'eshop_manual/eshop_manual_cn_v3.pdf';

    public function index()
    {
        $check = Storage::disk('oss')->exists(self::FILE_PATHNAME);

        return LaravelAdmin::content(function (Content $content) use ($check) {

            $content->header(__('merchant.manual.eshop_manual'));

            $content->breadcrumb(
                ['text' => __('merchant.manual.eshop_manual'), 'url' => 'store/eshop_manual', 'no-pjax' => 1]
            );

            $content->body(view('admin::eshop_manual.index')->with('file_exist', $check));
        });
    }

    public function take()
    {
        return Storage::disk('oss')->download(self::FILE_PATHNAME);
    }
}
