<?php

/*
 * This file is part of ibrand/backend.
 *
 * (c) iBrand <https://www.ibrand.cc>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace iBrand\Backend\Http\Controllers;

use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class ExportController extends Controller
{
    protected $cache;

    protected const HEADER_FORMAT = [
        'ADMIN' => [
            'S'     => '0.00',
            'U:W'   => '0.00',
            'Z:AE'  => '0.00',
            'AG:AK' => '0.00',
            'AO'    => '0.00',
        ],
        'MERCHANT' => [
            'Q:U'   => '0.00',
            'X:AA'  => '0.00',
            'AC:AF' => '0.00',
            'AJ'    => '0.00',
        ],
    ];

    public function __construct()
    {
        $this->cache = cache();
    }

    public function index()
    {
        $toggle = request('toggle');

        return view('admin::export.index', compact('toggle'));
    }

    public function downLoadFile()
    {
        Storage::makeDirectory('public/exports');
        $type   = request('type');
        $cache  = request('cache');
        $title  = explode(',', request('title'));
        $prefix = request('prefix');

        defined('MERCHANT_FORMAT') or define('MERCHANT_FORMAT', 41);
        $data = $this->cache->pull($cache);

        $fileName = generate_export_name($prefix);

        if (!Storage::disk('local')->exists('public/exports')) {
            Storage::disk('local')->makeDirectory('public/exports');
        }

        //  for export csv
        if ('csv' == $type) {
            $extension = '.csv';
        } else { //  for export excel
            set_time_limit(10000);
            ini_set('memory_limit', '1024M');
            $type      = 'xlsx';
            $extension = '.xlsx';
        }

        $header = count(explode(',', request('title', ''))) == 41 ? 'MERCHANT' : 'ADMIN';

        // to solve the floating bugs

        $column = SELF::HEADER_FORMAT[$header];

        Excel::create($fileName, function ($excel) use ($data, $title, $column) {
            $excel->sheet('Sheet1', function ($sheet) use ($data, $title, $column) {
                $sheet->setColumnFormat($column);
                $sheet->prependRow(1, $title);
                $sheet->rows($data);
            });
        })->store($type, storage_path('app/public/exports'), false);

        $result = \File::move(storage_path('app/public/exports/') . '/' . $fileName . $extension, storage_path('app/public/exports/') . $fileName . $extension);

        if ($result) {
            return $this->ajaxJson(true, ['url' => '/storage/exports/' . $fileName . $extension]);
        }

        return $this->ajaxJson(false);
    }

    public function setNoteRead()
    {
        $user = auth('admin')->user();
        $type = request('type');
        $user->unreadNotifications->where('type', $type)->where('created_at', '<', Carbon::now())->markAsRead();

        return $this->ajaxJson();
    }
}
