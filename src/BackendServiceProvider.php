<?php

/*
 * This file is part of ibrand/backend.
 *
 * (c) iBrand <https://www.ibrand.cc>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace iBrand\Backend;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use iBrand\Backend\Middleware\{Authenticate, Permission};
use iBrand\Backend\Console\{InstallCommand, InstallExtensionsCommand};

class BackendServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $commands = [
        InstallCommand::class,
        InstallExtensionsCommand::class,
    ];

    protected $namespace = 'iBrand\Backend\Http\Controllers';

    /**
     * Boot the service provider.
     */
    public function boot()
    {
        app('view')->prependNamespace('admin', __DIR__ . '/../resources/views');

        // merge configs
        $this->mergeConfigFrom(
            __DIR__ . '/../config/backend.php',
            'ibrand.backend'
        );

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/backend.php' => config_path('ibrand/backend.php'),
            ]);
            $this->publishes([__DIR__ . '/../config' => config_path()], 'laravel-admin-config');

            $this->publishes([__DIR__ . '/../resources/assets' => public_path('vendor')]);
        }

        //merge filesystem
        $this->setAdminDisk();

        $this->registerMigrations();

        $this->mapWebRoutes();

        $this->app['router']->aliasMiddleware('admin.auth', Authenticate::class);
        $this->app['router']->aliasMiddleware('supplier', LoginPage::class);
        $this->app['router']->aliasMiddleware('admin.permission', Permission::class);
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->commands($this->commands);
    }

    protected function setAdminDisk()
    {
        $filesystems = config('filesystems');

        $disks = array_merge($filesystems['disks'], config('ibrand.backend')['disks']);

        $filesystems['disks'] = $disks;

        $this->app['config']->set('filesystems', $filesystems);
    }

    /**
     * 数据迁移.
     */
    protected function registerMigrations()
    {
        return $this->loadMigrationsFrom(__DIR__ . '/../migrations');
    }

    protected function mapWebRoutes()
    {
        $attributes = [
            // 'prefix' => config('admin.route.prefix'),
            'namespace'  => $this->namespace,
            'middleware' => config('admin.route.middleware'),
        ];

        Route::group($attributes, function ($router) {
            $router->group([], function ($router) {
                $router->resource('users', 'UserController');

                $router->resource('menu', 'MenuController', ['except' => ['create']]);
            });

            $router->get('login', 'AuthAdminController@getLogin')->name('merchant.admin.login');

            $router->get('login', 'AuthAdminController@getLogin')->name('merchant.admin.login');

            $router->post('login', 'AuthAdminController@yippiPostLogin')->name('merchant.admin.login.post');

            $router->post('logout', 'AuthAdminController@getLogout');

            $router->post('admin/logout', 'AuthAdminController@getLogout');

            $router->get('setting', 'AuthAdminController@getSetting');

            $router->put('setting', 'AuthAdminController@putSetting');

            $router->get('admin/login', 'AuthAdminController@getLogin')->name('auth.admin.login');

            $router->post('admin/login', 'AuthAdminController@postLogin')->name('auth.admin.login.post');

            // route to login as
            $router->post('admin/loginas', 'AuthAdminController@loginAs')->name('auth.admin.loginas');

            // route to logout as
            $router->post('admin/logoutas', 'AuthAdminController@logoutAs')->name('auth.admin.logoutas');

            // route to change language
            $router->get('admin/changeLocale', 'AuthAdminController@changeLocale')->name('auth.admin.changeLocale');

            // route to save language
            $router->post('admin/saveLocale', 'AuthAdminController@saveLocale')->name('auth.admin.saveLocale');

            $router->get('store/user-policy-download', 'ManualController@index')->name('user.index.user_manual');

            $router->post('store/user-policy-download', 'ManualController@take')->name('user.take.user_manual');
        });

        Route::group(['namespace' => $this->namespace], function ($router) {
            $router->post('getMobile', 'AuthAdminController@getMobile');

            $router->group(['prefix' => 'export'], function () use ($router) {
                $router->get('/', 'ExportController@index')->name('admin.export.index');
                $router->get('downLoadFile', 'ExportController@downLoadFile')->name('admin.export.downLoadFile');
            });
        });
    }
}
