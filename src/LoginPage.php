<?php

namespace iBrand\Backend;

use Closure;

class LoginPage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(request()->isMethod('GET') && request()->has('lang')){
            app()->setLocale($request->lang);
        }
        return $next($request);
    }
}
