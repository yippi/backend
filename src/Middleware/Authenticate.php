<?php

namespace iBrand\Backend\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->guest() && !$this->shouldPassThrough($request)) {
            return redirect()->guest(admin_base_path('login'));
        }

        return $next($request);
    }

    /**
     * Determine if the request has a URI that should pass through verification.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    protected function shouldPassThrough($request)
    {
        $excepts = [
            admin_base_path('login'),
            admin_base_path('admin/login'),
            admin_base_path('logout'),
            admin_base_path('admin/logout'),
            'merchant/registration/*',
            '/tncs',
            '/privacy-policy',
            '/terms-and-conditions',
            '/seller-terms-and-conditions',
            'miniProgram/paymentGateway/*',
            'merchant/register/*',
            '/user-policy-download'
        ];

        foreach ($excepts as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->is($except)) {
                return true;
            }
        }

        return false;
    }
}
