<?php

namespace iBrand\Backend\Traits;

trait LoginPageable
{
    public function isPathToRedirect()
    {
        $url     = request()->getHttpHost().'/'.request()->path();
        $domains = config('admin.domains');

        if (in_array($url, $domains)) {
            return 'https://seller.topzmall.com/'.request()->path();
        }
    }

    public function isMerchantLogin()
    {
        if (str_contains(request()->route()->getName(), 'merchant.admin.login')) {
            return 'admin::merchant-login-ibrand';
        }
    }

    public function isAdminLogin()
    {
        if (! str_contains(request()->route()->getName(), 'merchant.admin.login')) {
            return 'admin::admin-login-ibrand';
        }
    }
}
