<?php

/*
 * This file is part of ibrand/backend.
 *
 * (c) iBrand <https://www.ibrand.cc>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace iBrand\Backend\Models;

use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Auth\Database\Administrator;

// use GuoJiangClub\Component\Supplier;
/**
 * Class Administrator.
 *
 * @property Role[] $roles
 */
class Admin extends Administrator{

    protected $fillable = ['username', 'name', 'avatar', 'email', 'yippi_username', 'supplier_id', 'main_role'];

    public function adminNotifications()
    {
        return $this->hasMany(AdminNotifications::class);
    }


}
