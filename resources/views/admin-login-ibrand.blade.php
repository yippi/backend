<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('admin.title')}} | {{ trans('admin.login') }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    {{-- <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css") }}"> --}}
    {!! Html::style(env("APP_URL").'/css/bootstrap-4.5.2-dist/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/font-awesome/css/font-awesome.min.css") }}">
    <!-- Theme style -->
    {{-- <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/AdminLTE/dist/css/AdminLTE.min.css") }}"> --}}
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/AdminLTE/plugins/iCheck/square/blue.css") }}">

    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/toastr/build/toastr.min.css") }}">

    <link rel="stylesheet" href="{{ admin_asset("/vendor/css/login.css") }}">

</head>
<div id="content">

    <div class="login-wrap">
        <div class="w clearfix">
            <div class="text-box">
                <div class="hello-box">
                    <div class="hello">
                        HELLO
                    </div>
                    <div>

                    </div>
                </div>
                <div class="info">
                    Topzmall Developed with Love
                </div>
            </div>

            <form id="login_form"  action="{{ admin_base_path('admin/login') }}" method="post" onsubmit="return check(this)">

                <div class="login-form">

                    <div class="login-box">
                        <div class="outer-logo">
                            <img class="logo" src="{{config('eshop.TopzMallImage.fullLogoUrl')}}" alt="">
                        </div>
                        <div class="p-3 bg-white rounded m-auto d-flex flex-column">
                            <span class="pb-3 m-auto font-weight-bold">Login</span>
                            <div class="inptu-box">

                                <input type="text" placeholder="Email/Username"  name="username" value="{{ old('username') }}">
                            </div>



                            <div class="inptu-box">

                                <input type="password" placeholder="Password" name="password">

                            </div>


                            @if(config('ibrand.backend.sms_login'))

                            <div class="inptu-box code-box">

                                <input type="text" placeholder="Verification code" name="code" value="{{ old('code') }}">

                                <button type="button" id="send-verifi" style="border: none" class="code" data-target="login" data-status=0>Send the verification code</button>

                            </div>

                            @endif


                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            {{--<div class="login">--}}
                                <button type="submit" class="login" id="login_button" onclick="() => $('#login_button').attr("disabled", true);"  >Login</button>
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </form>


        </div>
        <div class="login-banner">
            <div class="w">
                <div id="banner-bg" class="i-inner">
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>

<!-- jQuery 2.1.4 -->
{{-- <script src="{{ admin_asset("/vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js")}} "></script> --}}
<!-- Bootstrap 3.3.5 -->
{{-- <script src="{{ admin_asset("/vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js")}}"></script>
 --}}
 {!! Html::script(env("APP_URL").'/js/jquery-3.5.1-dist/jquery-3.5.1.min.js') !!}
{!! Html::script(env("APP_URL").'/js/popper-1.16.1-dist/popper.min.js') !!}
{!! Html::script(env("APP_URL").'/js/bootstrap-4.5.2-dist/bootstrap.min.js') !!}
<!-- iCheck -->
<script src="{{ admin_asset("/vendor/laravel-admin/AdminLTE/plugins/iCheck/icheck.min.js")}}"></script>

<script src="{{ admin_asset("/vendor/laravel-admin/toastr/build/toastr.min.js")}}"></script>

<script>


    const enable_button = () => $('#login_button').attr("disabled", false);

        function check(form) {

            form.login_button.disabled = true;

            if(form.username.value==''){
                toastr.warning("Please enter email or username");
                form.login_button.disabled = false;
                return false;
            }
            if(form.password.value==''){
                toastr.warning("Please enter the password");
                enable_button();
                return false;
            }
            return true;
        }

        @if($errors->has('username'))
        @foreach($errors->get('username') as $message)
        toastr.error("{{$message}}");
        @endforeach
        @endif

        @if($errors->has('code'))
        @foreach($errors->get('code') as $message)
        toastr.error("{{$message}}");
        @endforeach
        @endif

        @if($errors->has('password'))
        @foreach($errors->get('password') as $message)
        toastr.error("{{$message}}");
        @endforeach
        @endif

        window.AppUrl = "{{env('APP_URL')}}";
        window._token = "{{ csrf_token() }}";
        var postUrl = '{{env('APP_URL')}}/getMobile';

        @if(config('ibrand.backend.sms_login'))
        $(document).ready(function () {
            // 发送验证码
            $('#send-verifi').on('click', function () {
                var el = $(this);
                var target = el.data('target');
                var mobileReg = /^(?=\d{11}$)^1(?:3\d|4[57]|5[^4\D]|66|7[^249\D]|8\d|9[89])\d{8}$/;

                if(target == 'login'){ //  如果是登录
                    $.ajax({
                        type: 'post',
                        data: {
                            username: $('input[name="username"]').val(),
                            _token:_token
                        },
                        url: postUrl,
                        success: function (res) {
                            if (res.status) {
                                $('input[name="mobile"]').val(res.data.mobile);
                                sendCode(el,res.data.mobile);
                            } else {
                                toastr.error(res.msg);
                            }
                        },
                        error: function () {
                            toastr.error('Account verification failed');
                            enable_button();
                        }
                    })

                } else {
                    var mobile = $('input[data-type=' + target + ']').val();
                    if (mobile == ''){
                        enable_button();
                        toastr.error('Please enter the phone number');
                        return
                    }
                    if (!mobileReg.test(mobile)) {
                        enable_button();
                        toastr.error('Please enter the correct phone number');
                    } else {
                        sendCode(el,mobile);
                    }
                }
            });

            //发送验证码方法
            function sendCode(el,mobile) {
                if (el.data('status') != 0) {
                    return
                }
                el.text('sending...');
                el.data('status', '1');
                $.ajax({
                    type: 'POST',
                    data: {
                        mobile: mobile,
                        access_token: _token
                    },
                    url: AppUrl+"/{{config('ibrand.sms.route.prefix')}}/verify-code?_token="+_token,
                    success: function (data) {
                        if (data.success) {
                            $('input[name="access_token"]').val(_token);
                            var total = 60;
                            var message = 'Please wait{#counter#}second';
                            el.text(message.replace(/\{#counter#}/g, total));
                            var timer = setInterval(function () {
                                total--;
                                el.text(message.replace(/\{#counter#}/g, total));

                                if (total < 1) {
                                    el.data('status', '0');
                                    el.text('Send the verification code');
                                    clearInterval(timer);
                                }
                            },1000)
                        } else {
                            el.data('status', '0');
                            el.text('Send the verification code');
                            toastr.error('Message failed to send!');
                        }
                    },
                    error: function () {
                        el.data('status', '0');
                        el.text('Send the verification code');
                        toastr.error('Message failed to send!');
                    }
                })
            };

        });
    @endif

</script>

@include('admin::script');
