<script>
        @if($redirectTo)
            let url = "{!! $redirectTo !!}"
            toastr.options = {
                ...toastr.options,
                "positionClass": "toast-top-center",
                "preventDuplicates": false,
                "progressBar": true,
                "timeOut": "3000",
            }
            toastr.info("{!! __('merchant.login.redirect_to') !!}" + url);

            setTimeout(function(){
                window.location = url;
            }, 3000)
        @endif
</script>