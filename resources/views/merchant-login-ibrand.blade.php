<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('login.title')}} | {{ trans('admin.login') }}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- font-awesome 4.7.0 -->
    <link rel="stylesheet" href={{asset('/css/all.css')}}>
    {{--
    <!-- Mdn 4.4.1 -->--}}
    {!! Html::style(env("APP_URL").'/css/bootstrap-4.5.2-dist/bootstrap.min.css') !!}
    <link rel="stylesheet" type="text/css" href={{asset('/css/login.css')}}>
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/toastr/build/toastr.min.css") }}">
</head>
<form id="login_form">
    {{ csrf_field() }}
    <div class="row">
        <div class="login_page_background"></div>
        <div class="login_page_menu">
            <!-- Grid column -->
            <div class="m-auto login-box">

                <div class="d-flex flex-row-reverse">
                    <a href="{{URL::to('login?lang=en')}}"> &nbsp; EN </a>
                    |
                    <a href="{{URL::to('login?lang=zh-CN')}}">中文 &nbsp;</a>
                </div>

                <div class="outer-logo">
                    <img class="logo" src="{{config('eshop.TopzMallImage.fullLogoUrl')}}">
                </div>

                <div class="card-body d-flex flex-column p-3">
                    <span class="text-left pb-3 font-weight-bold">
                        <h5>{{__('merchant.yippi_login')}}</h5>
                    </span>
                    <!--Body-->
                    <div class="text-left mt-2"><label
                            class="login_menu_label font-weight-bold">{{__('merchant.login.username')}}</label></div>
                    <div class="input-group mb-3 md-form">
                        <input type="text" class="form-control input-box"
                            placeholder="{!! __('merchant.login.username') !!}" id="username" aria-label="Username"
                            aria-describedby="basic-addon1">
                    </div>
                    <div class="text-left"><label
                            class="login_menu_label font-weight-bold">{{__('merchant.login.password')}}</label></div>
                    <div class="md-form input-group">
                        <input type="password" id="password" placeholder="{!! __('merchant.login.password') !!}"
                            class="form-control input-box">
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="defaultCheck1">
                            {!! __('merchant.agree.merchant_login', ['seller_policy'=>
                            config('admin.seller_policy_redirect'), 'privacy_policy'=> config('admin.tnc_redirect')])
                            !!}
                        </label>
                    </div>


                    <div class="text-left mt-3">
                        <button type="submit" class="btn btn-primary login_button font-weight-bold"
                            disabled>{{__('merchant.sign_in')}}</button>
                    </div>
                    <div class="text-left mt-3">
                        <a class="btn btn-primary login_button font-weight-bold"
                            href={{route('merchant.register.index')}}>{{__('merchant.register_btn')}}</a>
                    </div>
                </div>
            </div>
        </div>
</form>

</html>


<style>
    .login_button:disabled {
        color: white !important;
        background-color: #cccccc !important;
    }
</style>

<!-- jQuery 2.1.4 -->
{{-- <script src={{asset('/js/jquery.min.js')}}></script> --}}
<!-- Bootstrap 3.3.5 -->
{{-- <script src="{{ admin_asset(" /vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js")}}"></script> --}}
{!! Html::script(env("APP_URL").'/js/jquery-3.5.1-dist/jquery-3.5.1.min.js') !!}
{!! Html::script(env("APP_URL").'/js/popper-1.16.1-dist/popper.min.js') !!}
{!! Html::script(env("APP_URL").'/js/bootstrap-4.5.2-dist/bootstrap.min.js') !!}
<!-- iCheck -->
<script src="{{ admin_asset("/vendor/laravel-admin/AdminLTE/plugins/iCheck/icheck.min.js")}}"></script>

<script src="{{ admin_asset("/vendor/laravel-admin/toastr/build/toastr.min.js")}}"></script>

<script>
    $(document).ready(function() {
        $(".form-check-input").on('click' , function() {
            let state = $('.login_button').prop('disabled');
            $('.login_button').prop('disabled', state);

            if(state){
                $('.login_button').removeAttr("disabled");
            }else{
                $('.login_button').attr("disabled", true);
            }
        });

		$('#login_form').submit(function($event) {
			const formData = {
				'username'            : $('input[id=username]').val(),
				'password'            : $('input[id=password]').val(),
            };

			$.ajax({
				headers: {
					'X-CSRF-Token': '{{ csrf_token() }}',
				},
				type: 'POST',
				url: "{{route('merchant.admin.login.post')}}",
				data: formData,
				dataType: 'json',
				success: function (result) {
					if (result.status) {
                        let base_url = window.location.origin;
                        window.location.href =  base_url + result.path
                        toastr.success('Login success')
                    }

                },
                error: function(res){
                    if(res.responseJSON.errors){
                        const message = Object.values(res.responseJSON.errors)[0];
                        toastr.error(message);

                        return;
                    }

                    if (res.responseJSON.code && res.responseJSON.code === 8) {
                        toastr.error('Incorrect username or password.');

                        return;
                    }

                    if (res.responseJSON.code) {
                        const message = res.responseJSON.message;
                        toastr.error(message);

                        return;
                    }
                    toastr.error('Login failed', res)
                }

            });

            $event.preventDefault();
         });
	});

</script>

@include('admin::script')