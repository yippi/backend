@extends('admin::layouts.bootstrap_modal')

@section('modal_class')
    modal-lg
@stop
@section('title')
    {{trans('admin.change_locale')}}
@stop

@section('body')
    <div class="">
        <form class="m-auto p-3 flex-grow-1" method="post" id="base-form"
              action="{{route('auth.admin.saveLocale')}}">
            {{csrf_field()}}
            <div class="col-md-10">
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">{{trans('admin.locale')}}</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="user_locale" name="user_locale">
                            @foreach($available_locale as $idx=>$locale)
                                <option value="{{$locale}}" @if(Admin::user()->user_locale == $locale || (!Admin::user()->user_locale && $locale == 'en')) selected @endif>{{trans('admin.'.$locale)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('footer')
    <button type="button" class="btn btn-link" data-dismiss="modal">取消</button>
    <button type="submit" class="btn btn-primary" data-toggle="form-submit" data-target="#base-form">提交</button>

    <script>
        $(document).ready(function () {
        });

        $(function () {
            $('#base-form').find("input").iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
                increaseArea: '10%'
            });
        });

        $('#base-form').ajaxForm({
            success: function (result) {
                if (result.status) {
                    swal({
                        title: "修改成功！",
                        text: "",
                        type: "success"
                    }, function () {
                        location.reload();
                    });
                }

            }
        });
    </script>
@stop