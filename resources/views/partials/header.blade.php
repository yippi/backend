<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:;"><i
                        class="fa fa-bars"></i> </a>
        </div>

        <ul class="nav navbar-nav top-navbar navbar-expand-lg mr-auto">
            @each('admin::partials.top-menu',BackendMenu::topMenu(), 'item')

        </ul>
        @if(config('ibrand.backend.scenario')=='normal' || !config('ibrand.backend.scenario'))
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a data-toggle="dropdown" class="dropdown-togg" href="graph_morris.html#">
                            <span class="block m-r-sm"> <strong class="font-bold">{{auth('admin')->user()->name}}<b
                                            class="caret"></b></strong>
                             </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        @if(session()->get('admin_check_supplier') != 1)
                            <li><a href="{{ admin_base_path('auth/setting') }}"><i class="fa fa-pencil-square-o"></i> {{ trans('admin.setting') }}</a></li>
                        @endif

                        @if(session()->get('master_admin_id') > 0)
                            <li><a class="logout_as" data-href="{{route('auth.admin.logoutas')}}"><i class="fa fa-sign-out"></i> {{ trans('admin.logout_as') }}</a></li>
                        @else
                            <li><a class="change_locale" data-toggle="modal" data-target="#modal" data-backdrop="static" data-keyboard="false" data-url="{{route('auth.admin.changeLocale')}}" href="javascript:;"><i class="fa fa-globe" aria-hidden="true"></i> {{ trans('admin.change_locale') }}</a></li>
                        @endif

                        @if(session()->get('admin_check_supplier') != 1 || session()->get('master_admin_id') > 0)
                            <li class="divider"></li>
                        @endif


                        <li>
                            <a href="{{ url('logout') }}"
                               onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> {{ trans('admin.logout') }}
                            </a>

                            <form id="logout-form" action="{{ url('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        @endif
    </nav>
</div>
<div id="modal" class="modal inmodal fade" data-keyboard=false data-backdrop="static"><div class="modal-dialog"></div></div>

<script>
    $(function () {
        $('.logout_as').on('click', function () {
            var logoutAsInfo = $(this);
            var logoutAsUrl = logoutAsInfo.data('href');

            $.post(logoutAsUrl, {token: _token}, function (result) {
                if (result.status) {
                    toastr.success(result.message);
                    location = '{{route('admin.store.dashboard.index')}}';
                } else {
                    toastr.error('{{trans('admin.failed_logout_as')}}');
                }
            });

            return;
        });
    });

    // for change language dialog
    $(document).on('click.modal.data-api', '[data-toggle="modal-filter"]', function (e) {
        var $this = $(this);
        modalUrl = $(this).data('url');

        if (modalUrl) {
            var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, '')));
            $target.modal('show');
            $target.html('').load(modalUrl, function () {

            });
        }
    });
</script>
