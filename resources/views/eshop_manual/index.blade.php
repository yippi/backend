<a href="https://youtube.com/playlist?list=PLNGIfnwTS6pLTFjt1JL_EGx6cNKmJl12Q" target="_blank" rel="noopener noreferrer" class="btn btn-primary mb-2"> Watch Tutorial In Youtube</a>

@if ($file_exist)
    <form method="POST" action="{{ route('user.take.user_manual') }}" class="downloadFile">
        {{ csrf_field() }}
        <input type="hidden" name="csrf_token">
        <input type="submit" class="btn btn-primary" value="Download eshop-v3-manual">
    </form>
@else
    <span>File not exist</span>
@endif
